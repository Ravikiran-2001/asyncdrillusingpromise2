const fs = require("fs").promises;
const problem1 = require("./problem1.js");
const problem2 = require("./problem2.js");
const problem3 = require("./problem3.js");

const boardFilePath = "../boards.json";
function problem6() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.readFile(boardFilePath, "utf8").then((file) => {
        const boardData = JSON.parse(file);
        const thanosBoard = boardData.find(
          (element) => element.name === "Thanos"
        );
        console.log("information from the Thanos boards :", thanosBoard);
        problem2(thanosBoard.id)
          .then((listsObject) => {
            const lists = Object.values(listsObject);
            console.log("All lists for the Thanos board:", lists);
            return lists;
          })
          .then((lists) => {
            const listsId = lists.flat().map((value) => problem3(value.id));
            return Promise.all(listsId);
          })
          .then((cardsArray) => {
            cardsArray.forEach((value) => {
              console.log(value);
            });
          })

          .catch((error) => {
            reject(error);
          });
      });
    }, 3000);
  });
}
module.exports = problem6;
