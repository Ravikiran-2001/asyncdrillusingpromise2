const fs = require("fs").promises;
const filePath = "../boards.json";
function boardInformation(boardId) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.readFile(filePath, "utf8")
        .then((data) => {
          const board = JSON.parse(data);
          const result = board.find((value) => value.id === boardId);
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    }, 3000);
  });
}
module.exports = boardInformation;
