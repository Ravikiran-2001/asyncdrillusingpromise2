const { resolve } = require("path");

const fs = require("fs").promises;
const filePath = "../cards.json";
function allCards(listId) {
  return new Promise((resolve, reject) => {
    let result = {};
    setTimeout(() => {
      fs.readFile(filePath, "utf8")
        .then((data) => {
          const list = JSON.parse(data);
          if (list.hasOwnProperty(listId)) {
            result[listId] = list[listId];
          }
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    }, 3000);
  });
}
module.exports = allCards;
