const fs = require("fs").promises;
const problem1 = require("./problem1.js");
const problem2 = require("./problem2.js");
const problem3 = require("./problem3.js");

const boardFilePath = "../boards.json";
function problem4() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.readFile(boardFilePath, "utf8").then((file) => {
        const boardData = JSON.parse(file);
        const thanosBoard = boardData.find(
          (element) => element.name === "Thanos"
        );
        console.log("information from the Thanos boards :", thanosBoard);
        problem2(thanosBoard.id)
          .then((listsObject) => {
            const lists = Object.values(listsObject);
            console.log("All lists for the Thanos board:", lists);
            return lists;
          })
          .then((lists) => {
            const mindId = lists.flat().find((list) => list.name === "Mind");
            if (mindId) {
              return problem3(mindId.id);
            }
          })
          .then((mindCards) => {
            console.log("all mind cards details", mindCards);
          })
          .catch((error) => {
            reject(error);
          });
      });
    }, 3000);
  });
}
module.exports = problem4;
