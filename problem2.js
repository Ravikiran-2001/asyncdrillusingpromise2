const fs = require("fs").promises;
const filePath = "../list.json";

function boardIdList(boardId) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fs.readFile(filePath, "utf8")
        .then((data) => {
          const list = JSON.parse(data);
          return list;
        })
        .then((list) => {
          if (list.hasOwnProperty(boardId)) {
            let result = {};
            result[boardId] = list[boardId];
            resolve(result);
          } else {
            reject("board id is not present");
          }
        })
        .catch((error) => {
          reject(error);
        });
    }, 3000);
  });
}
module.exports = boardIdList;
